class SessionInfo {

    constructor() {
        this.SessionInfo = {};
        this.loadData();
    }

    loadData() {
        var data = window.localStorage['kombos_v2_SessionInfo'];
        if (data && data != {} )
        {
            data = JSON.parse(data);
            this.SessionInfo = data;
        }
    }
    store(data) {
        window.localStorage.setItem("kombos_v2_SessionInfo", JSON.stringify(data));
    }

    info() {
        return this.SessionInfo;
    }
    set(data) {
        this.SessionInfo = data;
        this.store(data);

    }
}

export let sessioninfo = new SessionInfo();