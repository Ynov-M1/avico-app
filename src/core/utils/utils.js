
export const Utils = {
   pad: function(val)
    {
      var valString = val + "";
      if(valString.length < 2)
      {
          return "0" + valString;
      }
      else
      {
          return valString;
      }
    },
    time2html: function(since)
    {
        var sec = Math.trunc(since/1000.0);
        return ""+this.pad(parseInt(sec/60))+"m "+this.pad(sec%60)+"s";
    },
    compatibleDate: function(d_str)
    {
      if(d_str) {
        if( d_str.indexOf('Z') == -1)
          d_str = d_str+'Z';
        if( d_str.indexOf('T') == -1)
          d_str = d_str.split(" ").join("T");

        return new Date(d_str);
      }
      return new Date();
    },
    timeDisplay: function(time){
        var day;
        var hour;
        var min;
        var sec;
        time = Math.floor(time);
        sec = time % 60;
        sec = ('0' + sec).slice(-2);

        time = Math.floor((time-sec)/60);
        min = time % 60;
        min = ('0' + min).slice(-2);

        time = Math.floor((time-min)/60);
        hour = time % 24;
        hour = ('0' + hour).slice(-2);

        time = Math.floor((time-hour)/24);
        day = ('0' + time).slice(-2);
        return {sec, min, hour, day};
      }
}