class User {

    constructor() {
        this.userInfo = {};
        this.loadData();
    }

    loadData() {
        var data = window.localStorage['kombos_v2_user'];
        if (data && data != {} )
        {
            data = JSON.parse(data);
            this.userInfo = data;
        }
    }
    store(data) {
        window.localStorage.setItem("kombos_v2_user", JSON.stringify(data));
    }

    info() {
        return this.userInfo;
    }
    set(data) {
        this.userInfo = data;
        this.store(data);

    }
}

export let user = new User();