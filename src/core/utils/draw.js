export const Draw = {
   drawLine: function(x_from, x_to, y_from, y_to, ctx) {
      ctx.beginPath();
      ctx.moveTo(x_from, y_from);
      ctx.lineTo(x_to, y_to);
      ctx.stroke();
    },
    drawTo: function(x_to, y_to, ctxSpag) {
      //ctxSpag.beginPath();
      ctxSpag.lineWidth = 5;
      ctxSpag.strokeStyle = "#4D8BFF";
      ctxSpag.lineTo(x_to, y_to);
      ctxSpag.stroke();
    },
    drawPosition: function(x_to, y_to, ctxPos) {
      //ctxPos.clearRect(0,0,canvasWidth,canvasHeight);
      //ctxPos.beginPath();
      //console.log(x_to + " " + y_to);
      ctxPos.fillStyle = "grey";
      ctxPos.arc(x_to, y_to, 8, 0, 2 * Math.PI);
      ctxPos.fill();
    },
    colors:  {
            red: '#FF3C3C',
            lightgray: '#DDDDDD',
            yellow: '#FFAF3C',
            orange: '#FF8B3E',
            green: '#67CE67',
            blue: '#4D8BFF',
            gray: '#999999',
            dark: '#111111',
    }
}