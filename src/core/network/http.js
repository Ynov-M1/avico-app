import axios from 'axios';

export const http = axios.create({
 baseURL: process.env.API_URL,
 // baseURL: `http://127.0.0.1:9006/`,
 timeout: 2000,
 headers: {
   //Authorization: 'Bearer {token}'
 }
})
