import Vue from 'vue'
import App from './App.vue'
import VueOnsen from 'vue-onsenui' // This imports 'onsenui', so no need to import it separately

import 'onsenui/css/onsenui.css'
import 'onsenui/css/onsen-css-components.css'
import i18n from 'roddeh-i18n'
import french from './assets/french.json'

import { bus } from './core/utils/bus.js'
import { OnsenUtils } from './core/utils/onsen.js'

require('lodash')

/* eslint-disable no-new */

i18n.translator.add(french)
bus.vue.$on('french', () => {
  i18n.translator.add(french)
})

Vue.mixin({
  methods: {
    tr: function (key) {
      return i18n(key)
    },
    goTo: function (page) {
      OnsenUtils.bringPageTop(this.pageStack, page)
    }
  }
})

Vue.use(VueOnsen) // VueOnsen set here as plugin to VUE. Done automatically if a call to window.Vue exists in the startup code.
Vue.config.productionTip = false

new Vue({
  render: h => h(App)
}).$mount('#app')
